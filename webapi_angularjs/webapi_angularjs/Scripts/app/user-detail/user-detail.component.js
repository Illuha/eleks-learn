'use strict';

// Register `phoneDetail` component, along with its associated controller and template
angular.
  module('userDetail').
  component('userDetail', {
    templateUrl: 'user-detail/user-detail.template.html',
    controller: ['$routeParams', 'User',
      function PhoneDetailController($routeParams, Phone) {
        var self = this;
        self.user = User.get({phoneId: $routeParams.userId}, function(user) {
          //self.setImage(user.images[0]);
        });

        self.setImage = function setImage(imageUrl) {
          self.mainImageUrl = imageUrl;
        };
      }
    ]
  });
