'use strict';

// Register `userDetail` component, along with its associated controller and template
angular.
  module('userDetail').
  component('userDetail', {
    templateUrl: 'user-detail/user-detail.template.html',
    controller: ['$routeParams', 'User',
      function UserDetailController($routeParams, User) {
        var self = this;
        self.user = User.get({userId: $routeParams.userId}, function(user) {
        });

        self.setImage = function setImage(imageUrl) {
          self.mainImageUrl = imageUrl;
        };
      }
    ]
  });
