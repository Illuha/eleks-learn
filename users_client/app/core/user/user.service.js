'use strict';

angular.
  module('core.user').
  factory('User', ['$resource',
    function($resource) {
        return $resource('http://localhost:51591/api/ApplicationUsers/:userId', {}, {
        query: {
          method: 'GET',
          params: {userId: ''},
          isArray: true
        }
      });
    }
  ]);
