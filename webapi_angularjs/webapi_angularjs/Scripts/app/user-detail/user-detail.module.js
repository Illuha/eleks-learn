'use strict';

// Define the `phoneDetail` module
angular.module('userDetail', [
  'ngRoute',
  'core.user'
]);
