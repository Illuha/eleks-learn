﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UserService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUsersService
    {
        public List<ApplicationUser> GetAll()
        {
            List<ApplicationUser> userList = new List<ApplicationUser>();
            userList.Add(
                new ApplicationUser
                {
                    Id = 1,
                    Email = "user1@gmail.com",
                    UserName = "user1@gmail.com"
                }
            );

            userList.Add(
                new ApplicationUser
                {
                    Id = 2,
                    Email = "user2@gmail.com",
                    UserName = "user2@gmail.com"
                }
            );

            userList.Add(
                new ApplicationUser
                {
                    Id = 3,
                    Email = "user3@gmail.com",
                    UserName = "user3@gmail.com"
                }
            );
            return userList;
        }
    }
}
